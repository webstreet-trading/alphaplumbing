(function($){
    $(document).ready(function() {

        wp.customize('et_divi[sidebar]', function (value) {
            value.bind(function (to) {
                var $body = $('body');
                if (to) {
                    $body.removeClass('sidebar_disabled');
                }
                else {
                    $body.addClass('sidebar_disabled');
                }
            });
        });

    });
})(jQuery);
