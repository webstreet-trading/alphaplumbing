<?php if (!ws_get_epanel_option('_404_layout_divi_id')) : ?>
<div class="entry">
<!--If no results are found-->
	<h1><?php esc_html_e('404: No Results Found','ws'); ?></h1>
	<p><?php esc_html_e('The page you requested could not be found. Try refining your search, or use the navigation above to locate the page.','ws'); ?></p>
</div>
<!--End if no results are found-->
<?php else :
	echo ws_epanel_divi_layout('_404_layout_divi_id');
endif; ?>
