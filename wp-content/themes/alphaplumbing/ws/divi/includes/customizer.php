<?php

/**
 * Theme Customizer
 * 
 * @param object $wp_customize Customizer object
 */
function ws_customize_register($wp_customize) {
    
    /**
     * Login/Register
     * 
     * Based on Login Customizer by ThemeIsle (https://themeisle.com)
     */
    
    $wp_customize->add_panel('ws_login_panel', array(
        'priority' => 30,
        'capability' => 'edit_theme_options',
        'title' => __('Login Customizer', ws()),
        'description' => __('This section allows you to customize the login page of your website', ws()),
   ));

    $wp_customize->add_section('ws_login_logo_section', array(
        'priority' => 5,
        'title' => __('Logo', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_background_section', array(
        'priority' => 10,
        'title' => __('Background', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_form_bg_section', array(
        'priority' => 15,
        'title' => __('Form Background', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_form_section', array(
        'priority' => 20,
        'title' => __('Form Styling', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_field_section', array(
        'priority' => 25,
        'title' => __('Fields Styling', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_button_section', array(
        'priority' => 30,
        'title' => __('Button Styling', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_other_section', array(
        'priority' => 35,
        'title' => __('Other', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_section('ws_login_security_section', array(
        'priority' => 40,
        'title' => __('Security', ws()),
        'panel'  => 'ws_login_panel',
   ));

    $wp_customize->add_setting('ws_login_logo', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'ws_login_logo', array(
        'label' => __('Login Logo', ws()),
        'section' => 'ws_login_logo_section',
        'priority' => 5,
        'settings' => 'ws_login_logo'
   )));

    $wp_customize->add_setting('ws_login_logo_width', array(
        'default' => '84px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_logo_width', array(
        'label' => __('Logo Width', ws()),
        'section' => 'ws_login_logo_section',
        'priority' => 10,
        'settings' => 'ws_login_logo_width'
   ));

    $wp_customize->add_setting('ws_login_logo_height', array(
        'default' => '84px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_logo_height', array(
        'label' => __('Logo Height', ws()),
        'section' => 'ws_login_logo_section',
        'priority' => 15,
        'settings' => 'ws_login_logo_height'
   ));

    $wp_customize->add_setting('ws_login_logo_padding', array(
        'default' => '5px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_logo_padding', array(
        'label' => __('Padding Bottom', ws()),
        'section' => 'ws_login_logo_section',
        'priority' => 20,
        'settings' => 'ws_login_logo_padding'
   ));

    $wp_customize->add_setting('ws_login_bg_image', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'ws_login_bg_image', array(
        'label' => __('Background Image', ws()),
        'section' => 'ws_login_background_section',
        'priority' => 5,
        'settings' => 'ws_login_bg_image'
   )));

    $wp_customize->add_setting('ws_login_bg_color', array(
        'default' => '#F1F1F1',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_bg_color', array(
        'label' => __('Background Color', ws()),
        'section' => 'ws_login_background_section',
        'priority' => 10,
        'settings' => 'ws_login_bg_color'
   )));

    $wp_customize->add_setting('ws_login_bg_size', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_bg_size', array(
        'label' => __('Background Size', ws()),
        'section' => 'ws_login_background_section',
        'priority' => 15,
        'settings' => 'ws_login_bg_size'
   ));

    $wp_customize->add_setting('ws_login_form_bg_image', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'ws_login_form_bg_image', array(
        'label' => __('Background Image', ws()),
        'section' => 'ws_login_form_bg_section',
        'priority' => 5,
        'settings' => 'ws_login_form_bg_image'
   )));

    $wp_customize->add_setting('ws_login_form_bg_color', array(
        'default' => '#FFF',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_form_bg_color', array(
        'label' => __('Background Color', ws()),
        'section' => 'ws_login_form_bg_section',
        'priority' => 10,
        'settings' => 'ws_login_form_bg_color'
   )));

    $wp_customize->add_setting('ws_login_form_width', array(
        'default' => '320px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_form_width', array(
        'label' => __('Width', ws()),
        'section' => 'ws_login_form_section',
        'priority' => 15,
        'settings' => 'ws_login_form_width'
   ));

    $wp_customize->add_setting('ws_login_form_height', array(
        'default' => '194px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_form_height', array(
        'label' => __('Height', ws()),
        'section' => 'ws_login_form_section',
        'priority' => 20,
        'settings' => 'ws_login_form_height'
   ));

    $wp_customize->add_setting('ws_login_form_padding', array(
        'default' => '26px 24px 46px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_form_padding', array(
        'label' => __('Padding', ws()),
        'section' => 'ws_login_form_section',
        'priority' => 25,
        'settings' => 'ws_login_form_padding'
   ));

    $wp_customize->add_setting('ws_login_form_border', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_form_border', array(
        'label' => __('Border (Example: 2px dotted black) ', ws()),
        'section' => 'ws_login_form_section',
        'priority' => 30,
        'settings' => 'ws_login_form_border'
   ));

    $wp_customize->add_setting('ws_login_field_width', array(
        'default' => '100%',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_field_width', array(
        'label' => __('Input Field Width', ws()),
        'section' => 'ws_login_field_section',
        'priority' => 5,
        'settings' => 'ws_login_field_width'
   ));

    $wp_customize->add_setting('ws_login_field_margin', array(
        'default' => '2px 6px 16px 0px',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_field_margin', array(
        'label' => __('Input Field Margin', ws()),
        'section' => 'ws_login_field_section',
        'priority' => 10,
        'settings' => 'ws_login_field_margin'
   ));

    $wp_customize->add_setting('ws_login_field_bg', array(
        'default' => '#FFF',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_field_bg', array(
        'label' => __('Input Field Background', ws()),
        'section' => 'ws_login_field_section',
        'priority' => 15,
        'settings' => 'ws_login_field_bg'
   )));

    $wp_customize->add_setting('ws_login_field_color', array(
        'default' => '#333',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_field_color', array(
        'label' => __('Input Field Color', ws()),
        'section' => 'ws_login_field_section',
        'priority' => 20,
        'settings' => 'ws_login_field_color'
   )));

    $wp_customize->add_setting('ws_login_field_label', array(
        'default' => '#777',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_field_label', array(
        'label' => __('Label Color', ws()),
        'section' => 'ws_login_field_section',
        'priority' => 25,
        'settings' => 'ws_login_field_label'
   )));

    $wp_customize->add_setting('ws_login_button_bg', array(
        'default' => '#2EA2CC',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_bg', array(
        'label' => __('Button Background', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 5,
        'settings' => 'ws_login_button_bg'
   )));

    $wp_customize->add_setting('ws_login_button_border', array(
        'default' => '#0074A2',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_border', array(
        'label' => __('Button Border', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 10,
        'settings' => 'ws_login_button_border'
   )));

    $wp_customize->add_setting('ws_login_button_hover_bg', array(
        'default' => '#1E8CBE',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_hover_bg', array(
        'label' => __('Button Background (Hover)', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 15,
        'settings' => 'ws_login_button_hover_bg'
   )));

    $wp_customize->add_setting('ws_login_button_hover_border', array(
        'default' => '#0074A2',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_hover_border', array(
        'label' => __('Button Border (Hover)', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 20,
        'settings' => 'ws_login_button_hover_border'
   )));

    $wp_customize->add_setting('ws_login_button_shadow', array(
        'default' => '#78C8E6',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_shadow', array(
        'label' => __('Button Box Shadow', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 25,
        'settings' => 'ws_login_button_shadow'
   )));
    
    $wp_customize->add_setting('ws_login_button_text_shadow', array(
        'default' => '#78C8E6',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_text_shadow', array(
        'label' => __('Button Text Shadow', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 25,
        'settings' => 'ws_login_button_text_shadow'
   )));

    $wp_customize->add_setting('ws_login_button_color', array(
        'default' => '#FFF',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_button_color', array(
        'label' => __('Button Color', ws()),
        'section' => 'ws_login_button_section',
        'priority' => 30,
        'settings' => 'ws_login_button_color'
   )));

    $wp_customize->add_setting('ws_login_other_color', array(
        'default' => '#999',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_other_color', array(
        'label' => __('Text Color', ws()),
        'section' => 'ws_login_other_section',
        'priority' => 5,
        'settings' => 'ws_login_other_color'
   )));

    $wp_customize->add_setting('ws_login_other_color_hover', array(
        'default' => '#2EA2CC',
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'ws_login_other_color_hover', array(
        'label' => __('Text Color (Hover)', ws()),
        'section' => 'ws_login_other_section',
        'priority' => 10,
        'settings' => 'ws_login_other_color_hover'
   )));

    $wp_customize->add_setting('ws_login_other_css', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
   ));

    $wp_customize->add_control('ws_login_other_css', array(
        'label' => __('Custom CSS', ws()),
        'type' => 'textarea',
        'section' => 'ws_login_other_section',
        'priority' => 15,
        'settings' => 'ws_login_other_css'
   ));
    
    /**
     * END Login/Register
     */
    
    /**
     * Layout options
     */
    
    $wp_customize->add_setting('et_divi[sidebar]', array(
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage',
        'sanitize_callback' => 'wp_validate_boolean',
        'default' => true,
    ));

    $wp_customize->add_control('et_divi[sidebar]', array(
        'label' => esc_html__('Enable Sidebar', ws()),
        'section' => 'et_divi_general_layout',
        'type' => 'checkbox',
        'priority' => 1,
    ));

    /**
     * END Layout options
     */
    
    /**
     * Additional theme colors define
     */
    
    $wp_customize->add_setting('et_divi[accent_color_alt_1]', array(
        'default' => '#ffffff',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage',
        'sanitize_callback' => 'et_sanitize_alpha_color',
    ));

    $wp_customize->add_control(new ET_Divi_Customize_Color_Alpha_Control($wp_customize, 'et_divi[accent_color_alt_1]', array(
        'label' => esc_html__('Theme Accent Color Alt', ws()),
        'section' => 'et_divi_general_layout',
        'settings' => 'et_divi[accent_color_alt_1]',
    )));
    
    $wp_customize->add_setting('et_divi[accent_color_alt_2]', array(
        'default' => '#ffffff',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage',
        'sanitize_callback' => 'et_sanitize_alpha_color',
    ));
    
    $wp_customize->add_control(new ET_Divi_Customize_Color_Alpha_Control($wp_customize, 'et_divi[accent_color_alt_2]', array(
        'label' => esc_html__('Theme Accent Color Alt 2', ws()),
        'section' => 'et_divi_general_layout',
        'settings' => 'et_divi[accent_color_alt_2]',
    )));
    
    $wp_customize->add_setting('et_divi[accent_color_alt_3]', array(
        'default' => '#ffffff',
        'type' => 'option',
        'capability' => 'edit_theme_options',
        'transport' => 'postMessage',
        'sanitize_callback' => 'et_sanitize_alpha_color',
    ));
    
    $wp_customize->add_control(new ET_Divi_Customize_Color_Alpha_Control($wp_customize, 'et_divi[accent_color_alt_3]', array(
        'label' => esc_html__('Theme Accent Color Alt 3', ws()),
        'section' => 'et_divi_general_layout',
        'settings' => 'et_divi[accent_color_alt_3]',
    )));
}
add_action('customize_register', 'ws_customize_register');

/**
 * Login/Register Customizer output
 */
function ws_login_customizer() {
    $logo_url = get_option('ws_login_logo');
    $logo_width = get_option('ws_login_logo_width');
    $logo_height = get_option('ws_login_logo_height');
    $logo_padding = get_option('ws_login_logo_padding');
    $bg_img = get_option('ws_login_bg_image');
    $bg_color = get_option('ws_login_bg_color');
    $bg_size = get_option('ws_login_bg_size');
    $form_bg_image = get_option('ws_login_form_bg_image');
    $form_bg_color = get_option('ws_login_form_bg_color');
    $form_width = get_option('ws_login_form_width');
    $form_height = get_option('ws_login_form_height');
    $form_padding = get_option('ws_login_form_padding');
    $form_border = get_option('ws_login_form_border');
    $field_width = get_option('ws_login_field_width');
    $field_margin = get_option('ws_login_field_margin');
    $field_bg = get_option('ws_login_field_bg');
    $field_color = get_option('ws_login_field_color');
    $field_label = get_option('ws_login_field_label');
    $button_bg = get_option('ws_login_button_bg');
    $button_border = get_option('ws_login_button_border');
    $button_shadow = get_option('ws_login_button_shadow');
    $button_text_shadow = get_option('ws_login_button_text_shadow');
    $button_color = get_option('ws_login_button_color');
    $button_hover_bg = get_option('ws_login_button_hover_bg');
    $button_hover_border = get_option('ws_login_button_hover_border');
    $other_color = get_option('ws_login_other_color');
    $other_color_hover = get_option('ws_login_other_color_hover');
    $other_css = get_option('ws_login_other_css');
?>
<style type="text/css">
    html, body {
<?php if(!empty($bg_img)) : ?>
    background-image: url(<?php echo $bg_img; ?>) !important;
<?php endif; ?>
<?php if(!empty($bg_color)) : ?>
    background-color: <?php echo $bg_color; ?> !important;
<?php endif; ?>
<?php if(!empty($bg_url)) : ?>
    background-size: <?php echo $bg_size; ?> !important;
<?php endif; ?>
    }
    body.login div#login h1 a {
<?php if(!empty($logo_url)) : ?>
    background-image: url(<?php echo $logo_url; ?>) !important;
<?php endif; ?>
<?php if(!empty($logo_width)) : ?>
    width: <?php echo $logo_width; ?> !important;
<?php endif; ?>
<?php if(!empty($logo_height)) : ?>
    height: <?php echo $logo_height; ?> !important;
<?php endif; ?>
<?php if(!empty($logo_width) || !empty($logo_height)) : ?>
    background-size: <?php echo $logo_width; ?> <?php echo $logo_height; ?> !important;
<?php endif; ?>
<?php if(!empty($logo_padding)) : ?>
    padding-bottom: <?php echo $logo_padding; ?> !important;
<?php endif; ?>
}
#loginform {
<?php if(!empty($form_bg_image)) : ?>
    background-image: url(<?php echo $form_bg_image; ?>) !important;
<?php endif; ?>
<?php if(!empty($form_bg_color)) : ?>
    background-color: <?php echo $form_bg_color; ?> !important;
<?php endif; ?>
<?php if(!empty($form_height)) : ?>
    height: <?php echo $form_height; ?> !important;
<?php endif; ?>
<?php if(!empty($form_padding)) : ?>
    padding: <?php echo $form_padding; ?> !important;
<?php endif; ?>
<?php if(!empty($form_border)) : ?>
    border: <?php echo $form_border; ?> !important;
<?php endif; ?>
}
#login {
<?php if(!empty($form_width)) : ?>
    width: <?php echo $form_width; ?> !important;
<?php endif; ?>
}
.login form .input, .login input[type="text"] {
<?php if(!empty($field_width)) : ?>
    width: <?php echo $field_width; ?> !important;
<?php endif; ?>
<?php if(!empty($field_margin)) : ?>
    margin: <?php echo $field_margin; ?> !important;
<?php endif; ?>
<?php if(!empty($field_bg)) : ?>
    background: <?php echo $field_bg; ?> !important;
<?php endif; ?>
<?php if(!empty($field_color)) : ?>
    color: <?php echo $field_color; ?> !important;
<?php endif; ?>
}
.login label {
<?php if(!empty($field_label)) : ?>
    color: <?php echo $field_label; ?> !important;
<?php endif; ?>
}
.wp-core-ui .button-primary {
<?php if(!empty($button_bg)) : ?>
    background: <?php echo $button_bg; ?> !important;
<?php endif; ?>
<?php if(!empty($button_border)) : ?>
    border-color: <?php echo $button_border; ?> !important;
<?php endif; ?>
<?php if(!empty($button_shadow)) : ?>
    box-shadow: 0px 1px 0px <?php echo $button_shadow; ?> inset, 0px 1px 0px rgba(0, 0, 0, 0.15) !important;
<?php endif; ?>
<?php if(!empty($button_text_shadow)) : ?>
    text-shadow: 0 -1px 1px <?php echo $button_text_shadow; ?>,1px 0 1px <?php echo $button_text_shadow; ?>,0 1px 1px <?php echo $button_text_shadow; ?>,-1px 0 1px <?php echo $button_text_shadow; ?>  !important;
<?php endif; ?>
<?php if(!empty($button_color)) : ?>
    color: <?php echo $button_color; ?> !important;
<?php endif; ?>
}
.wp-core-ui .button-primary.focus, .wp-core-ui .button-primary.hover, .wp-core-ui .button-primary:focus, .wp-core-ui .button-primary:hover {
<?php if(!empty($button_hover_bg)) : ?>
    background: <?php echo $button_hover_bg; ?> !important;
<?php endif; ?>
<?php if(!empty($button_hover_border)) : ?>
    border-color: <?php echo $button_hover_border; ?> !important;
<?php endif; ?>
}
.login #backtoblog a, .login #nav a {
<?php if(!empty($other_color)) : ?>
    color: <?php echo $other_color; ?> !important;
<?php endif; ?>
}
.login #backtoblog a:hover, .login #nav a:hover, .login h1 a:hover {
<?php if(!empty($other_color_hover)) : ?>
    color: <?php echo $other_color_hover; ?> !important;
<?php endif; ?>
}
<?php if(!empty($other_css)) : ?>
    <?php echo $other_css; ?>
<?php endif; ?>
</style>
<?php
}
add_action('login_enqueue_scripts', 'ws_login_customizer');

/**
 * Change Login/Register logo url to blog url
 * 
 * @return string URL to link to
 */
function ws_login_customizer_logo_url() {
    return get_bloginfo('url');
}
add_filter('login_headerurl', 'ws_login_customizer_logo_url');

/**
 * Change Login/Register logo url title to blog name
 * 
 * @return string Text to use
 */
function ws_login_customizer_logo_url_title() {
    $title = get_bloginfo('name', 'display');
    return $title;
}
add_filter('login_headertext', 'ws_login_customizer_logo_url_title');

/**
 * Enqueue javascript for customizer
 */
function ws_divi_customize_preview_js() {
    
    wp_enqueue_script('ws-customizer', get_stylesheet_directory_uri() . '/assets/js/ws-customizer.js', array('customize-preview'), ws('version'), true);
}
add_action('customize_preview_init', 'ws_divi_customize_preview_js');

/**
 * Save customizer relevant variables to sass file for use
 * 
 * @param WP_Customize_Manager $manager
 */
function ws_customize_save_sass_variables($manager) {
    
    // sass paths
    $sass_directory = get_stylesheet_directory() . '/assets/sass/partials/';
    $sass_filepath = $sass_directory . '_variables_generated.scss';
    
    // ensure file exits, if not create it
    if (wp_mkdir_p($sass_directory) && !file_exists($sass_filepath)) {
        $file_handle = @fopen($sass_filepath, 'w');
        @fclose($file_handle);
    }
    
    // open file for writing
    $file_handle = @fopen($sass_filepath, 'w+');
    if (!$file_handle) {
        return;
    }
    
    // get content to write
    $sass_content = ws_customize_sass_variables_get_content();
    
    // write content to file
    @fwrite($file_handle, $sass_content);
    
    // close file
    @fclose($file_handle);
}
add_action('customize_save_after', 'ws_customize_save_sass_variables');

/**
 * Get sass variables content
 * 
 * @return string
 */
function ws_customize_sass_variables_get_content() {
    
    // get variables
    $content_width = et_get_option('content_width', '1080');
    
    // colors
    $accent_color = et_get_option('accent_color', '#2ea3f2');
    $accent_color_alt_1 = et_get_option('accent_color_alt_1', '#ffffff');
    $accent_color_alt_2 = et_get_option('accent_color_alt_2', '#ffffff');
    $accent_color_alt_3 = et_get_option('accent_color_alt_3', '#ffffff');
    $body_font_color = et_get_option('font_color', '#666666');
    $link_color = et_get_option('link_color', $accent_color);
    
    // fonts
    $body_header_size = absint(et_get_option('body_header_size', '30'));
    $body_font_size = absint(et_get_option('body_font_size', '14'));
    $heading_font = sanitize_text_field(et_pb_get_specific_default_font(et_get_option('heading_font', 'none')));
    $body_font = sanitize_text_field(et_pb_get_specific_default_font(et_get_option('body_font', 'none')));
    $body_font_height = floatval(et_get_option('body_font_height', '1.7'));
    $body_header_height = floatval(et_get_option('body_header_height', '1'));
    $body_header_style = et_get_option('body_header_style', '', '', true);

    // buttons
    $button_text_size = absint(et_get_option('all_buttons_font_size', '20'));
    $button_text_color = et_get_option('all_buttons_text_color', '#ffffff');
    $button_bg_color = et_get_option('all_buttons_bg_color', 'rgba(0,0,0,0)');
    $button_border_width = absint(et_get_option('all_buttons_border_width', '2'));
    $button_border_color = et_get_option('all_buttons_border_color', '#ffffff');
    $button_border_radius = absint(et_get_option('all_buttons_border_radius', '3'));
    $button_text_style = et_get_option('all_buttons_font_style', '', '', true);
    $button_icon = et_get_option('all_buttons_selected_icon', '5');
    $button_spacing = intval(et_get_option('all_buttons_spacing', '0'));
    $button_icon_color = et_get_option('all_buttons_icon_color', '#ffffff');
    $button_text_color_hover = et_get_option('all_buttons_text_color_hover', '#ffffff');
    $button_bg_color_hover = et_get_option('all_buttons_bg_color_hover', 'rgba(255,255,255,0.2)');
    $button_border_color_hover = et_get_option('all_buttons_border_color_hover', 'rgba(0,0,0,0)');
    $button_border_radius_hover = absint(et_get_option('all_buttons_border_radius_hover', '3'));
    $button_spacing_hover = intval(et_get_option('all_buttons_spacing_hover', '0'));
    $button_icon_size = 1.6 * intval($button_text_size);
    $et_gf_buttons_font = sanitize_text_field(et_get_option('all_buttons_font', 'none'));
    $button_icon_placement = et_get_option('all_buttons_icon_placement', 'right');
    $button_icon_on_hover = et_get_option('all_buttons_icon_hover', 'yes');
    $button_use_icon = et_get_option('all_buttons_icon', 'yes');

    // calculations/logic
    
    if ('none' == $heading_font) {
        $heading_font = 'Open Sans';
    }
    if ('none' == $body_font) {
        $body_font = 'Open Sans';
    }
    if ('none' == $et_gf_buttons_font) {
        $et_gf_buttons_font = 'Open Sans';
    }
    
    if (in_array('bold', $body_header_style)) {
        $heading_font_weight = 'bold';
    }
    else {
        $heading_font_weight = 'normal';
    }
    
    if ('5' !== $button_icon) {
        $button_icon_size = $button_text_size;
    }
    else {
        $button_icon = '\35';
    }
    if ("'" === $button_icon) {
        $button_icon_content = '"' . htmlspecialchars_decode($button_icon) . '"';
    }
    else {
        $button_icon_content = "'" . htmlspecialchars_decode($button_icon) . "'";
    }

    ob_start();
?>
/******************************************************************
Stylesheet: Generated Variables

Sass variables generated from theme customizer.

******************************************************************/

/*********************
LAYOUT
*********************/

$content_width:                 <?php echo $content_width; ?>px;

/*********************
COLORS
*********************/

$color_accent:                  <?php echo $accent_color; ?>;
$color_accent_2:                <?php echo $accent_color_alt_1; ?>;
$color_accent_3:                <?php echo $accent_color_alt_2; ?>;
$color_accent_4:                <?php echo $accent_color_alt_3; ?>;
$color_text:                    <?php echo $body_font_color; ?>;
$color_link:                    <?php echo $link_color; ?>;

/*********************
BUTTONS
*********************/

$button_text_size:              <?php echo $button_text_size; ?>px;
$button_text_color:             <?php echo $button_text_color; ?>;
$button_bg_color:               <?php echo $button_bg_color; ?>;
$button_border_width:           <?php echo $button_border_width; ?>px;
$button_border_color:           <?php echo $button_border_color; ?>;
$button_border_radius:          <?php echo $button_border_radius; ?>px;
$button_icon_content:           <?php echo $button_icon_content; ?>;
$button_spacing:                <?php echo $button_spacing; ?>px;
$button_icon_color:             <?php echo $button_icon_color; ?>;
$button_text_color_hover:       <?php echo $button_text_color_hover; ?>;
$button_bg_color_hover:         <?php echo $button_bg_color_hover; ?>;
$button_border_color_hover:     <?php echo $button_border_color_hover; ?>;
$button_border_radius_hover:    <?php echo $button_border_radius_hover; ?>px;
$button_spacing_hover:          <?php echo $button_spacing_hover; ?>;
$button_icon_size:              <?php echo $button_icon_size; ?>px;
$button_icon_placement:         '<?php echo $button_icon_placement; ?>';
$button_icon_on_hover:          '<?php echo $button_icon_on_hover; ?>';
$button_use_icon:               '<?php echo $button_use_icon; ?>';

$button_text_style_map:         <?php echo sanitize_text_field(ws_et_pb_print_font_style_map($button_text_style)); ?>;
$button_font_family:            <?php echo sanitize_text_field(ws_et_builder_get_font_family($et_gf_buttons_font)); ?>;

/*********************
FONTS
*********************/

$header_size:                   <?php echo $body_header_size; ?>px;
$font_size:                     <?php echo $body_font_size; ?>px;

$heading_font:                  <?php echo sanitize_text_field(ws_et_builder_get_font_family($heading_font)); ?>;
$body_font:                     <?php echo sanitize_text_field(ws_et_builder_get_font_family($body_font)); ?>;

$heading_line_height:           <?php echo $body_header_height; ?>;
$body_line_height:              <?php echo $body_font_height; ?>;

$heading_font_weight:           <?php echo $heading_font_weight; ?>;

<?php
    return ob_get_clean();
}

/**
 * Get ET Builder font family css value for specific font
 * 
 * @param string $font_name
 * @return string CSS Font family
 */
function ws_et_builder_get_font_family($font_name) {
    
    $user_fonts = et_builder_get_custom_fonts();
    $fonts = isset($user_fonts[$font_name]) ? $user_fonts : et_builder_get_fonts();
    $removed_fonts_mapping = et_builder_old_fonts_mapping();

    $font_name_ms = isset($fonts[$font_name]) && isset($fonts[$font_name]['add_ms_version']) ? "'{$font_name} MS', " : "";

    if (isset($removed_fonts_mapping[$font_name]) && isset($removed_fonts_mapping[$font_name]['parent_font'])) {
        $font_name = $removed_fonts_mapping[$font_name]['parent_font'];
    }

    return sprintf('\'%1$s\', %3$s%2$s',
		esc_html($font_name),
		isset($fonts[$font_name]) ? et_builder_get_websafe_font_stack($fonts[$font_name]['type']) : 'sans-serif',
		$font_name_ms
	);
}

/**
 * Get ET Builder array font style css value for font style
 *
 * @return array
 */
function ws_et_pb_print_font_style_map($styles = '', $important = '') {

    // Prepare variable
    $font_styles = [];

    if ('' !== $styles && false !== $styles) {
        // Convert string into array
        $styles_array = explode('|', $styles);

        // If $important is in use, give it a space
        if ($important && '' !== $important) {
            $important = " " . $important;
        }

        // Use in_array to find values in strings. Otherwise, display default text
        // Font weight
        if (in_array('bold', $styles_array)) {
            $font_styles['font-weight'] = "bold{$important}";
        }
        else {
            $font_styles['font-weight'] = "normal{$important}";
        }

        // Font style
        if (in_array('italic', $styles_array)) {
            $font_styles['font-style'] = "italic{$important}";
        }
        else {
            $font_styles['font-style'] = "normal{$important}";
        }

        // Text-transform
        if (in_array('uppercase', $styles_array)) {
            $font_styles['text-transform'] = "uppercase{$important}";
        }
        else {
            $font_styles['text-transform'] = "none{$important}";
        }

        // Text-decoration
        if (in_array('underline', $styles_array)) {
            $font_styles['text-decoration'] = "underline{$important}";
        }
        else {
            $font_styles['text-decoration'] = "none{$important}";
        }
    }
    
    // convert to sass map
    $font_styles_map_array = [];
    foreach ($font_styles as $key => $value) {
        $font_styles_map_array[] = $key . ': ' . $value;
    }
    
    $font_styles_map = '(' . implode(', ', $font_styles_map_array) . ')';

    return $font_styles_map;
}
