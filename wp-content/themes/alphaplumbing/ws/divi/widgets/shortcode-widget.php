<?php
/**
 * Shortcode Widget
 */
class WS_Shortcode_Widget extends WP_Widget {

    /**
     * Constructor
     */
	public function __construct() {
		$widget_ops	 = array('classname' => 'ws_shortcode_widget', 'description' => __('Plain text widget that can handle shortcodes', ws()));
		$control_ops = array('width' => 400, 'height' => 350);
		parent::__construct('ws_shortcode_widget', __('WS Shortcode Widget', ws()), $widget_ops, $control_ops);
	}
    
    /**
     * Output widget
     * 
     * @param array $args Widget arguments
     * @param array $instance Widget instance
     */
	public function widget($args, $instance) {
        
		if (!empty($instance['title'])) {
            $instance['title'] = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);
        }
        
		echo $args['before_widget'];
        
		if (!empty($instance['title'])) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		
        echo '<div class="ws-shortcode-widget-content">' . do_shortcode($instance['shortcode']) . '</div>';
        
		echo $args['after_widget'];
	}

	/**
     * Update widget
     * 
     * @param array $new_instance New instance
     * @param array $old_instance Old instance
     * @return array Instance to use
     */
	public function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if (current_user_can('unfiltered_html')) {
			$instance['shortcode'] = $new_instance['shortcode'];
        }
		else {
			$instance['shortcode'] = stripslashes(wp_filter_post_kses(addslashes($new_instance['shortcode'])));
        }
		return $instance;
	}

	/**
	 * @param array $instance
	 */
	public function form($instance) {
		$instance = wp_parse_args((array) $instance, array('title' => '', 'shortcode' => ''));
		$title = strip_tags($instance['title']);
		$text = esc_textarea($instance['shortcode']);
		?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', ws()); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('shortcode'); ?>"><?php _e('Shortcode:'); ?></label>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('shortcode'); ?>" name="<?php echo $this->get_field_name('shortcode'); ?>"><?php echo $text; ?></textarea></p>
		<?php
	}

}
