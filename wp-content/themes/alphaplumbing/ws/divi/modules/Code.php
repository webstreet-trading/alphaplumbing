<?php

class WS_Builder_Module_Code_View extends ET_Builder_Module_Code {

	/**
     * The string used to uniquely identify
     *
     * @var string
     */
    public $textdomain = null;

	function init() {
		parent::init();
		$this->name            = esc_html__( 'Code View', 'et_builder' );
		$this->slug            = 'ws_pb_code_view';

		$this->textdomain = WS::$textdomain;
		$this->whitelisted_fields[] = 'template';
		$this->fields_defaults['template'] = ['default.php'];

	}

	function get_fields() {

		// get templates
        $available_templates = WS::directory_files(dirname(WS::get_viewpath('modules/code/default.php')));
        $templates = array_combine($available_templates, $available_templates);

		$fields = array(
			'template' => array(
				'label'           => esc_html__( 'View', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'basic_option',
				'options'		  => $templates,
				'description'     => esc_html__( 'Select your custom view.', 'et_builder' ),
				'is_fb_content'   => true,
				'toggle_slug'     => 'main_content',
			),
		);

		return $fields;
	}

    protected function _render_module_wrapper( $output = '', $render_slug = '' ) {
		return $output;
	}

    function render( $attrs, $content = null, $render_slug ) {
		$video_background			= $this->video_background();
		$parallax_image_background	= $this->get_parallax_image_background();
		$template					= $this->props['template'];
        $template_classname         = 'code_' . str_replace('.php', '', $template);
		$this->content				= ws_view('modules/code/' . $template);

		// Module classnames
		$this->add_classname( $this->get_text_orientation_classname() );
        $this->add_classname( $template_classname );

		$output = sprintf(
			'<div%2$s class="%3$s">
				%5$s
				%4$s
				<div class="et_pb_code_inner %6$s_inner">
					%1$s
				</div> <!-- .et_pb_code_inner -->
			</div> <!-- .et_pb_code -->',
			$this->content,
			$this->module_id(),
			$this->module_classname( $render_slug ),
			$video_background,
			$parallax_image_background,
            $template_classname
		);

		return $output;
	}
}

new WS_Builder_Module_Code_View;
